# Latex Gitlab Ci Template

Simple gitlab-ci template for latex projects featuring building, linting and language checking.

<!-- TOC -->
* [Latex Gitlab Ci Template](#latex-gitlab-ci-template)
  * [Features](#features)
  * [Configuration-Options](#configuration-options)
  * [Output](#output)
  * [Referenced Files](#referenced-files)
<!-- TOC -->

## Features

- Compiling latex documents to pdf
- Checking the latex document for errors using [textidote](https://github.com/sylvainhalle/textidote)
    - Grammar and Spell checking using [LanguageTool](https://languagetool.org/)
    - Latex linting

## Configuration-Options

You can set them in the `variables` section in [.gitlab-ci.yml](.gitlab-ci.yml)

**MAIN_FILE**

Main file of the document to be compiled to pdf. The default is `src/main.tex`.

**LANGUAGE**

Language used for grammar and spell checking. The default is `de`.

**allow_failure**

If set to `true` the pipeline will not fail if the latex document contains errors. The default is `true`.
Can be found at the end of the test job. 

## Output

The results of the pipeline can be downloaded in the Gitlab Artifacts as `build` (pdf) and `test` (language and latex
reports).

![](gitlab-artifacts.png)

## Referenced Files

In order to make the language and latex checks work, you need to add "magic" latex comments annotating the root file in
the beginning of any referenced files other than the main file.

`included.tex:`
```latex
%!TEX root = main.tex


\section{Included Section}\label{sec:included-section}
```